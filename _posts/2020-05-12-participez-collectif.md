---
layout: post
title: "Participer au collectif interhop.org"
permalink: /participez-collectif
ref: participez-collectif
lang: fr
---

**interhop.org** promeut l'interopérabilité et et le partage libre et sécurisé d'algorithme.

Vous voulez participer à la construction d'une informatique médicale libre / opensource, décentralisée et interopérable ?

<!-- more -->

Remplissez ce formulaire : [forms.interhop.org](https://forms.interhop.org/node/19)

On vous recontacte :-)
Bien sur ca n'engage en rien.
