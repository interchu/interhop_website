---
layout: post
title: "Covid, Health Data and Microsoft"
permalink: /covid-donnees-de-sante-microsoft_en
ref: covid-donnees-de-sante-microsoft
lang: en
---

> "Legal Europe must wake up, driven by France and the pressure of public opinion. It must propose a third way to guarantee a digital future compatible with our democracies," called for in this forum-petition many health and digital professionals, among others. A link is online to sign it.

<!-- more -->

[Sign this article](https://forms.interhop.org/node/20)

## Loyal and informed information

The digital literacy rate - the ability to read and write computer language - is extraordinarily low[^alphabetisation]. Our technophile world is flying into complexity, leaving the citizen out of the political debate. However, the implications are fundamental: they concern the sustainability of our freedom of thought and our mutualist health system.<br>
We want to inform about recent developments in information technology and health to refocus the debate politically around the citizen and close the door to techno-scientific illusions.

## Towards shared professional secrecy

Recently, the French Medical Association reminded that the secrecy of persons is the basis of the trust placed in health care personnel[^CNOM_secret].<br>
Historically, the care relationship is built on a singular doctor-patient colloquium.<br>
With the arrival of new technologies and the digitisation of the world, formal medical secrecy is gradually disappearing. State administrative staff already process the medical information of the insured of the Assurance Maladie[^Revel]. Contemporary developments tend to multiply the number of players in the healthcare process in a logic of collective and multidisciplinary care. How can this ethical requirement be reconciled with the 21st century, at a time of massive data processing carried out by increasingly sophisticated algorithms?

The notions[^macsf] "care team" and "shared secret" redefine the contours of secrecy in the digital age. Medical information that is strictly "necessary for continuity of care" can circulate between the different members of the care team to improve the overall management of the person. Moreover, opposition to data sharing[^opposition] is an essential right for the patient. The prerequisite for any refusal is the right to clear and precise information on the expected benefits, the forces involved and the course of the medical data. The digital divide[^fracnum] and the eagerness to deploy IT tools no longer allow these rights to be guaranteed.

## Technophily and centralization

"Science without conscience is the ruin of the soul." Rabelais

It is a fundamental ethical principle: "technologies must be at the service of the individual and society" rather than "enslaved by technological giants"[^CNOM_IA].

Blind confidence in technology, and in particular in new statistical tools, could lead to the legitimization of pyramid schemes and potentially freedom-reducing systems.<br>
Thus, the new information systems initiated in the context of the state of emergency are all centralised: collection of identities and risk contacts, blood test results, surveillance of mobile phones.
These systems will feed the national health data platform or "Health Data Hub"[^theconversation_Fallery]. This one-stop shop for access to all de-identified health data aims to develop artificial intelligence applied to health. All this data is hosted by the Microsoft company and its commercial offer, the Azure platform[^HDH_microsoft].

The problem is that American law applies to the whole world!<br>
In 2018, the American government adopted a text called "Cloud Act"[^cloudact], which allows the American justice to have access to data stored in third countries[^CCBE_cloud_act]. Microsoft is subject to this text which is in conflict with our European data protection regulation[^CNIL_denis].

How can we support this choice when the President of the National Agency for the Security of Information Systems publicly opposes the digital giants which would represent an attack on our "mutualist health" systems[^ANSSI]?<br>
How can we support this choice when the CNIL mentions in the contract linking the "Health Data Hub" to Microsoft "the existence of data transfers outside the European Union as part of the platform's day-to-day operation"[^CNIL_microsoft]?<br>
How can we support this choice when there are dozens of French and European industrial alternatives[^HDS]?

## Third way: autonomy / Europe

Brutally, the Snowden case showed the use of our data through globalized monitoring programs[^snowden_europ].<br>
Brutally, confinement has made us live in our flesh with imposed and necessary deprivations of liberty.

At the heart of the economy of the 21st century, data has gradually taken on crucial importance. They are the oil of our modern economies[^theeconomist] and the one who controls them, imposes himself. They are exploited by platform states dependent on market forces (Google, Apple, Facebook, Amazon, Microsoft) or authoritarian regimes (Baidu, Alibaba, Tencent and Xiaomi)[^internet_chinois].

We need to go back to basic political principles[^prefiguration] and understand that "the health data heritage is a national asset... The sovereignty and independence of our health system from foreign interests, as well as the competitiveness of our research and industry, will depend on how quickly France grasps the issue."

The autonomy of individuals must be strengthened. The provision of enlightened and transparent information bringing together patients, healthcare professionals and legislators must be achieved. Then people will be able to oppose the sending of data concerning them outside the legal framework that defends them.

Legal Europe must wake up, driven by France and the pressure of public opinion. It must propose a third way to guarantee a digital future compatible with our democracies. "The fundamental challenge for Europeans is to be able to maintain their autonomy of thought."[^liberty]<br>
It is therefore up to European and French legislators to protect democracy in the age of surveillance capitalism. The Court of Justice of the European Union[^cjue] as well as national personal data regulators must take a stand on the possibility of contractualization with companies subject to US laws.

The Franco-German initiative GAIA-X[^gaiax], which wants to provide a technical framework of transparency and good conduct to globalized platform states, must be propelled by the European Union. This is an absolute necessity.<br>
For an autonomous Digital Europe, it is necessary to use "Free/Libre software and open formats in the development"[^rep_num] of information systems. To ensure that everyone has access to quality care in the future, let us demand the publication of architecture plans for IT platforms, data flows, algorithms and medical terminologies.

In addition, the quality of research is co-constructed with multidisciplinary teams and interactions between care and research. On the spot, within hospitals and structures involved in public service, the "commonality" and autonomy of a fabric of teachers, researchers, computer scientists, carers and a network of people in trust with the whole system must be encouraged. Within this network, the role of the regions and hospitals must be strengthened.

If the legal and theoretical framework is to come from Europe, compliance with reality will remain local.


---

[Signing this paper](https://forms.interhop.org/node/20)

[See original article in Médiapart](https://blogs.mediapart.fr/les-invites-de-mediapart/blog/270520/le-covid-les-donnees-de-sante-et-microsoft)

---

#### Summary
- Formal medical secrecy -> secret shared with the care team and reinforcement of professional secrecy
  - Singular Colloquium -> Multiple actors
- In all cases SIDEP, Contact trancing, HDH... : centralization
  - All at Microsoft
  - Presentation of the ANSSI and CNIL opinion
- Solutions:
  - Autonomy of people: information and opposition
  - Legal autonomy: CJEU -> opinion of the CNIL (French National Commission for Information Technology and Civil Liberties)
  - Technical autonomy: GAIAX, Autonomous Digital Europe
  - Local and common autonomy: hospitals, region

#### Sources

[^HDS]: [Annuaire des hébergeurs aggréés AFHADS](https://www.afhads.fr/wp-content/uploads/2018/05/6-Annuaire-des-membres.pdf)

[^theconversation_Fallery]: [Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^alphabetisation]: [L'alphabétisation numérique et la participation familiale à l'école](https://www.cairn.info/revue-la-revue-internationale-de-l-education-familiale-2014-1-page-55.htm)

[^CNOM_secret]: [Plan de déconfinement et garantie du secret médical](https://www.conseil-national.medecin.fr/publications/communiques-presse/plan-deconfinement-garantie-secret-medical)

[^theeconomist]: [The world’s most valuable resource is no longer oil, but data](https://www.economist.com/leaders/2017/05/06/the-worlds-most-valuable-resource-is-no-longer-oil-but-data)

[^macsf]: [Comment concilier respect du secret professionnel et efficacité des soins ?](https://www.macsf.fr/Responsabilite-professionnelle/Relation-au-patient-et-deontologie/concilier-secret-professionnel-efficacite-des-soins)

[^préfiguration]: [HealthDataHub : Mission de préfiguration](https://solidarites-sante.gouv.fr/IMG/pdf/181012_-_rapport_health_data_hub.pdf)

[^Revel]: ["Le secret médical sera préservé"](https://www.franceinter.fr/emissions/l-invite-du-week-end/l-invite-du-week-end-10-mai-2020)

[^opposition]: [Opposition et information](https://www.cnil.fr/fr/respecter-les-droits-des-personnes)

[^CNOM_IA]: [Médecins et Patients dans le monde des data, des algorithmes et de l'intelligence artificielle](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)

[^internet_chinois]: ["Enquête. Quand Internet sera chinois"](https://pad.interhop.org/MmdhzZ6hS0KfGJUkfzSoqw#)

[^HDH_microsoft]: [Modalités de stockage du « health data hub »](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^snowden_europ]: [Trente-cinq chefs d'État étaient sous écoute de la NSA](https://www.lepoint.fr/monde/trente-cinq-chefs-d-etat-etaient-sous-ecoute-de-la-nsa-24-10-2013-1747689_24.php)

[^gaiax]: [Franco-German Position on GAIA-X](https://www.bmwi.de/Redaktion/DE/Downloads/F/franco-german-position-on-gaia-x.pdf?__blob=publicationFile&v=10)

[^libertépenser]: ["Thomas Gomart (IFRI) : « Le Covid-19 accélère le changement de mains de pans entiers de l’activité économique au profit des plateformes numériques »"](https://pad.interhop.org/94WL-H95QCaJv5vBKJzm4g)

[^cjue]: [Données personnelles : le transfert vers les Etats-Unis validé par la CJUE](https://www.lesechos.fr/tech-medias/hightech/donnees-personnelles-le-transfert-vers-les-etats-unis-valide-par-la-cjue-1157955)

[^cloudact]: [Rapport Gauvain : Rétablir la souveraineté de la France et de l’Europe et protéger nos entreprises des lois et mesures à portée extraterritoriale](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf)

[^CNIL_denis]: [Commission spéciale Bioéthique : Auditions diverses, Mme DENIS](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543)

[^CCBE_cloud_act]: [Évaluation du CCBE de la loi CLOUD Act des États-Unis](https://www.ccbe.eu/fileadmin/speciality_distribution/public/documents/SURVEILLANCE/SVL_Position_papers/FR_SVL_20190228_CCBE-Assessment-of-the-U-S-CLOUD-Act.pdf)

[^ANSSI]: [Audition de M. Guillaume Poupard, directeur général de l'Agence nationale de la sécurité des systèmes d'information (ANSSI)](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)

[^CNIL_microsoft]: [Délibération n° 2020-044 du 20 avril 2020 portant avis sur un projet d'arrêté complétant l’arrêté du 23 mars 2020 prescrivant les mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^rep_num]: [LOI n° 2016-1321 du 7 octobre 2016 pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=id)

[^fracnum]: [Les quatre dimensions de la fracture numérique](https://www.cairn.info/revue-reseaux1-2004-5-page-181.htm)
