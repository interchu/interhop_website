---
layout: post
title: "Mr Stallman, Thank You"
permalink: /stallman-merci_en
ref: stallman-merci
lang: en
---

Richard Stallman, thank you for signing our [article](https://interhop.org/covid-donnees-de-sante-microsoft) in Médiapart Journal.

On the subject of digital sovereignty and autonomy, Richard Stallman published an [article](https://www.gnu.org/government/): "Measures Governments Can Use to Promote Free Software And why it is their duty to do so".

<!-- more -->

> "Each generation has its philosopher, writer or artist who captures the spirit of the times. Sometimes these philosophers are recognized as such; often it takes generations before they are recognized. But recognized or not, an era is still marked by people talking about their ideals, in the murmur of a poem or the explosion of a political movement.<br><br>
> Our generation has a philosopher. He is neither an artist nor a professional writer. He's a programmer. Richard Stallman began his work in the MIT labs as a programmer and operating system architect. He built his career in the public arena as a programmer and founding architect of a movement for freedom in a world increasingly defined by "code".

> [Lawrence Lessig](https://www.gnu.org/philosophy/lessig-fsfs-intro.fr.html), Professor of Law, Stanford Law School
