---
layout: post
title: "Participate in the interhop.org collective"
permalink: /participez-collectif_en
ref: participez-collectif
lang: en
---

**interhop.org** promotes interoperability and free and secure algorithm sharing.

Do you want to participate in the construction of free / opensource, decentralized and interoperable medical IT ?

<!-- more -->

Fill out this form:  [forms.interhop.org](https://forms.interhop.org/node/19)

We will get back to you :-) Of course, this is not binding.
