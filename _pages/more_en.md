---
layout: default_en
permalink: more_en
lang: en
ref: more
---

# Available services

[forum.interhop.org](http://forum.interhop.org) : InterHop's instant messaging tool

[susana.interhop.org](http://susana.interhop.org) : International web application for managing terminologies

[my.interhop.org](http://my.interhop.org) : managing and reading articles with refined content

[links.interhop.org](http://links.interhop.org) : wall of useful links

[git.interhop.org](http://git.interhop.org) : InterHop git

[pad.interhop.org](http://pad.interhop.org) : word processing and MarkDown [presentations](http://pad.interhop.org/p/HJN5MhVjH#/)

[calc.interhop.org](http://calc.interhop.org) :  spreadsheet

[draw.interhop.org](http://draw.interhop.org) : diagram creation application

[drop.interhop.org](http://drop.interhop.org) : file repository with url

[paste.interhop.org](http://paste.interhop.org) : text extract manager (or source code)

# Is that all ?
Feel free to ask for new services if you need them

Founders **interhop.org**' collective:
- Florent DESGRIPPES, Engineer
- Adrien PARROT, Intensivist / Engineer
- Nicolas PARIS, bigData Engineer
- Antoine LAMER, Datascientist

Thanks to Miguel Ángel Armengol de la Hoz for the help with the design of the logo :-)

### Contact us by email

[InterHop](mailto:interhop@riseup.net)
