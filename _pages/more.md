---
layout: default
permalink: more
lang: fr
ref: more
---

# Services disponibles
[forum.interhop.org](http://forum.interhop.org) : outil de messagerie instantanée d'InterHop

[susana.interhop.org](http://susana.interhop.org) : application web nationale d'interopérabilité sémantique

[my.interhop.org](http://my.interhop.org) : gestion et lecture d'articles avec un contenu épuré

[links.interhop.org](http://links.interhop.org) : mur de liens utiles

[git.interhop.org](http://git.interhop.org) : git d'InterHop

[pad.interhop.org](http://pad.interhop.org) : traitement de texte et de [présentations](http://pad.interhop.org/p/HJN5MhVjH#/) en MarkDown

[calc.interhop.org](http://calc.interhop.org) : feuille de calcul

[draw.interhop.org](http://draw.interhop.org) : application de création de diagrammes

[drop.interhop.org](http://drop.interhop.org) : dépot de fichiers avec url

[paste.interhop.org](http://paste.interhop.org) : gestionnaire d'extraits de texte (ou code source)

# C'est tout ?
N'hésitez pas à demander des nouveaux services si vous en avez besoin!

Bureau  d'**interhop.org** :
- Adrien PARROT, Anesthésiste-réanimateur / ingénieur, président
- Antoine LAMER, Datascientist, trésorier
- Nicolas PARIS, Ingénieur bigData, secrétaire

Sympathisants :
- Florent DESGRIPPES, Ingénieur informaticien
- Jamel LOUCHI, médecin, libriste
- Niels MARTIGNENE, médecin, libriste
- Quentin PARROT, informaticien

Merci A Miguel Ángel Armengol de la Hoz pour l'aide pour le design du logo :-)

### Contactez nous par mail

[InterHop](mailto:interhop@riseup.net)
